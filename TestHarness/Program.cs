﻿using Com.Poyn.POSBridgeClient.Model;
using Com.Poynt.Api.Model;
using Com.Poynt.PosBridgeClient.Model;
using Com.Poynt.POSBridgeClient.Utils;
using Jil;
using RestSharp;
using System;
using System.IO;
using System.Web.Script.Serialization;

namespace TestHarness
{
    public class Program
    {
        private static string _host = "http://10.255.140.195:55555";
        private static string _pairingPath = "/devices/cat/pair";
        private static string _authorizeSalePath = "/devices/cat/authorizeSales";
        private static string _getScanDataPath = "/devices/imagescanner/getScanData";
        private static int _timeout = 60000; // deault timeout 60 seconds
        private static string _pairingCode = "ZHED2L";

        public static void Main(string[] args)
        {
            //
            // POS/Poynt pairing request
            //
            PairPosWithPoynt();

            //
            // Sale request example
            //
            RequestSale();

            //
            // Barcode scan example
            //           
            ScanBarcode();
        }

        private static void RequestSale()
        {
            string signature;
            POSRequest salePosRequest = new POSRequest();
            Payment payment = new Payment();
            payment.amount = 1000;
            payment.currency = "USD";
            // to allow multiple tenders on a single transaction
            payment.multiTender = true;
            // set custom reference id (could be POS reference)
            payment.referenceId = Guid.NewGuid().ToString();

            salePosRequest.payment = payment;
            // for idempotency, has to be UUID
            salePosRequest.poyntRequestId = Guid.NewGuid().ToString();
            // set client timeout to 60 seconds
            salePosRequest.timeout = _timeout;

            string saleRequest = JSON.Serialize<POSRequest>(salePosRequest, Options.ISO8601);


            signature = CryptoHelper.GenerateHMACSha256Signature(saleRequest, _pairingCode);
            string saleResponse = DoPost(_host, signature, saleRequest, _authorizeSalePath, _timeout, salePosRequest.poyntRequestId);

            POSResponse salePosResponse;

            using (var input = new StringReader(saleResponse))
            {
                salePosResponse = JSON.Deserialize<POSResponse>(input, Options.ISO8601);
            }

            if (salePosResponse.error != null)
            {
                Console.WriteLine("Error occurred: " + salePosResponse.error.errorName);
            }
            else
            {
                Payment processedPayment = salePosResponse.payment;
                // successful payment
                if (processedPayment.status == PaymentStatus.COMPLETED)
                {
                    foreach (Transaction t in processedPayment.transactions)
                    {
                        Console.WriteLine("--------------------");
                        Console.WriteLine("Amount: " + t.amounts.transactionAmount);
                        Console.WriteLine("Txn Id: " + t.id);

                        if (t.processorResponse != null)
                        {
                            Console.WriteLine("Approval Code: " + t.processorResponse.approvalCode);
                        }

                        if (t.fundingSource.card != null)
                        {
                            Console.WriteLine("Customer: " + t.fundingSource.card.cardHolderFullName);
                            Console.WriteLine("Card: " + t.fundingSource.card.numberMasked);
                        }

                    }
                }
                else if (processedPayment.status == PaymentStatus.CANCELED)
                {
                    Console.WriteLine("Payment was canceled");
                }
                else
                {
                    // just print the response
                    Console.WriteLine("Response: " + saleResponse);
                }
            }
        }

        private static void ScanBarcode()
        {
            string poyntRequestId, signature, response;
            poyntRequestId = Guid.NewGuid().ToString();
            // pairing request format
            string getScanDataRequest = "{\"poyntRequestId\":\"" + poyntRequestId + "\"}";
            signature = CryptoHelper.GenerateHMACSha256Signature(getScanDataRequest, _pairingCode);

            response = DoPost(_host, signature, getScanDataRequest, _getScanDataPath, _timeout, poyntRequestId);
            Console.WriteLine(response);
        }

        private static void PairPosWithPoynt()
        {
            string poyntRequestId, signature, response;
            poyntRequestId = Guid.NewGuid().ToString();
            string pairingRequest = "{\"poyntRequestId\":\"" + poyntRequestId + "\"}";
            signature = CryptoHelper.GenerateHMACSha256Signature(pairingRequest, _pairingCode);
            response = DoPost(_host, signature, pairingRequest, _pairingPath, _timeout, poyntRequestId);
            Console.WriteLine(response);
            Console.ReadLine();
        }

        public static string DoPost(string host, string signature, string jsonRequest, string uriString, int timeout, string requestId = "")
        {
            // This method utilizes RestSharp library
            // Please visit http://restsharp.org/ for documentation
            // Any other library can be used as an alternative, as well
            // as native .Net HttpRequest call

            string responseString = string.Empty;

            if (string.IsNullOrEmpty(requestId))
            {
                requestId = Guid.NewGuid().ToString();
            }

            var client = new RestClient(host);
            var request = new RestRequest(uriString, Method.POST);

            request.AddHeader("Poynt-Client", ".Net Sample");
            request.AddHeader("Poynt-Request-Id", requestId);
            request.AddHeader("Signature", signature);

            request.RequestFormat = DataFormat.Json;
            request.AddParameter("text/plain", jsonRequest, ParameterType.RequestBody);
            request.ReadWriteTimeout = timeout;

            IRestResponse response = client.Execute(request);

            //Return raw content as string
            return response.Content;
        }
    }
}
