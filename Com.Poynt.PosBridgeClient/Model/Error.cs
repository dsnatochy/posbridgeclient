﻿using System.Runtime.Serialization;

namespace Com.Poyn.POSBridgeClient.Model
{
    [DataContract]
    public class Error
    {
        [DataMember(IsRequired = false)]
        public int errorCode { get; set; }
        [DataMember(IsRequired = false)]
        public string errorName { get; set; }
        [DataMember(IsRequired = false)]
        public string description { get; set; }
    }
}
