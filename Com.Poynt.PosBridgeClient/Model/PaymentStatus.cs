﻿namespace Com.Poynt.PosBridgeClient.Model
{
    public enum PaymentStatus
    {
        COMPLETED,
        AUTHORIZED,
        VOIDED,
        REFUNDED,
        CANCELED,
        FAILED,
        HOLD,
        DECLINED
    }
}
