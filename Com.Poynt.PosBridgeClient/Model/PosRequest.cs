﻿using System.Collections.Generic;
using Com.Poynt.Api.Model;
using System.Runtime.Serialization;

namespace Com.Poynt.PosBridgeClient.Model
{
    [DataContract]
    public class POSRequest
    {
        [DataMember(IsRequired = false)]
        public PrintingRequest printingRequest { get; set; }

        [DataMember(IsRequired = false)]
        public SecondScreenRequest secondScreenRequest { get; set; }

        [DataMember(IsRequired = false)]
        public SettlementRequest settlementRequest { get; set; }

        [DataMember(IsRequired = false)]
        public int timeout { get; set; }

        [DataMember(IsRequired = false)]
        public string transactionId { get; set; }

        [DataMember(IsRequired = false)]
        public string poyntRequestId { get; set; }

        [DataMember(IsRequired = false)]
        public CATRequest catRequest { get; set; }

        [DataMember(IsRequired = false)]
        public Payment payment { get; set; }
    }

    [DataContract]
    public class CATRequest
    {
        [DataMember(IsRequired = false)]
        public int amount { get; set; }

        [DataMember(IsRequired = false)]
        public int sequenceNumber { get; set; }

        [DataMember(IsRequired = false)]
        public int taxOthers { get; set; }

        [DataMember(IsRequired = false)]
        public int timeout { get; set; }

        [DataMember(IsRequired = false)]
        public int type { get; set; }
    }

    [DataContract]
    public class PrintingRequest
    {
        [DataMember(IsRequired = false)]
        public string content { get; set; }
    }

    [DataContract]
    public class SecondScreenRequest
    {
        [DataMember(IsRequired = false)]
        public List<OrderItem> items { get; set; }

        [DataMember(IsRequired = false)]
        public long totalAmount { get; set; }

        [DataMember(IsRequired = false)]
        public string currency { get; set; }
    }

    [DataContract]
    public class SettlementRequest
    {
        [DataMember(IsRequired = false)]
        public bool closeBatch { get; set; }

        [DataMember(IsRequired = false)]
        public List<string> authorizationList { get; set; }
    }

}
