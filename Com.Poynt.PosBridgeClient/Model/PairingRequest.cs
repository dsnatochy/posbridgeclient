﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Com.Poynt.PosBridgeClient.Model
{
    [DataContract]
    public class PairingRequest
    {
        [DataMember(IsRequired = false)]
        public string poyntRequestId { get; set; }
    }
}
