using Com.Poynt.PosBridgeClient.Model;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
    [DataContract]
    public class Payment
	{
        [DataMember(IsRequired = false)]
        public long amount {get; set;}
        [DataMember(IsRequired = false)]
        public long tipAmount {get; set;}
        [DataMember(IsRequired = false)]
        public string currency {get; set;}
        [DataMember(IsRequired = false)]
        public string referenceId {get; set;}
        [DataMember(IsRequired = false)]
        public PaymentStatus status {get; set;}
        [DataMember(IsRequired = false)]
        public List<Transaction> transactions {get; set;}
        [DataMember(IsRequired = false)]
        public string transactionNumber {get; set;}
        [DataMember(IsRequired = false)]
        public bool multiTender {get; set;}
        [DataMember(IsRequired = false)]
        public bool authzOnly {get; set;}
        [DataMember(IsRequired = false)]
        public string orderId {get; set;}
        [DataMember(IsRequired = false)]
        public bool nonReferencedCredit {get; set;}
        [DataMember(IsRequired = false)]
        public bool disableDebitCards {get; set;}
        [DataMember(IsRequired = false)]
        public bool disableCash {get; set;}
        [DataMember(IsRequired = false)]
        public List<TransactionReference> references {get; set;}
        [DataMember(IsRequired = false)]
        public bool disableTip {get; set;}
        [DataMember(IsRequired = false)]
        public string notes {get; set;}
        [DataMember(IsRequired = false)]
        public Order order {get; set;}
        [DataMember(IsRequired = false)]
        public long cashbackAmount {get; set;}
        [DataMember(IsRequired = false)]
        public bool cashOnly {get; set;}
        [DataMember(IsRequired = false)]
        public bool debit {get; set;}
        [DataMember(IsRequired = false)]
        public bool skipReceiptScreen {get; set;}
        [DataMember(IsRequired = false)]
        public string sTAN {get; set;}
	}
}
