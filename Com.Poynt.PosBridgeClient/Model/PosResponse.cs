﻿using Com.Poyn.POSBridgeClient.Model;
using Com.Poynt.Api.Model;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Com.Poynt.PosBridgeClient.Model
{
    [DataContract]
    public class POSResponse
    {
        [DataMember(IsRequired = false)]
        public string poyntRequestId { get; set; }

        [DataMember(IsRequired = false)]
        public Error error { get; set; }

        [DataMember(IsRequired = false)]
        public Payment payment { get; set; }

        [DataMember(IsRequired = false)]
        public CATResponse catResponse { get; set; }
    }

    [DataContract]
    public class CATResponse
    {
        [DataMember(IsRequired = false)]
        public int sequenceNumber { get; set; }

        [DataMember(IsRequired = false)]
        public string accountNumber { get; set; }

        [DataMember(IsRequired = false)]
        public string approvalCode { get; set; }

        [DataMember(IsRequired = false)]
        public string cardCompanyID { get; set; }

        [DataMember(IsRequired = false)]
        public string cardHolderFirstName { get; set; }

        [DataMember(IsRequired = false)]
        public string cardHolderLastName { get; set; }

        [DataMember(IsRequired = false)]
        public string centerResultCode { get; set; }

        [DataMember(IsRequired = false)]
        public int paymentCondition { get; set; }

        [DataMember(IsRequired = false)]
        public string transactionNumber { get; set; }

        [DataMember(IsRequired = false)]
        public int transactionType { get; set; }
    }

    [DataContract]
    public class SettlementResponse
    {
        [DataMember(IsRequired = false)]
        public static List<string> acceptedAuthorizations;
    }

}
