﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Com.Poynt.POSBridgeClient.Utils
{
    public class CryptoHelper
    {
        public static string GenerateHMACSha256Signature(string dataToSign, string secretKey)
        {
            //Basic data and key verification
            if(string.IsNullOrEmpty(dataToSign) || string.IsNullOrEmpty(secretKey))
            {
                throw new ArgumentNullException("Data and Key can't be null");
            }

            byte[] keyBytes = Encoding.UTF8.GetBytes(secretKey);
            byte[] dataBytes = Encoding.UTF8.GetBytes(dataToSign);
            byte[] signatureBytes;

            using (HMACSHA256 hmac = new HMACSHA256(keyBytes))
            {
                signatureBytes = hmac.ComputeHash(dataBytes);
            }

            return ByteArrayToHex(signatureBytes);
        }

        private static byte[] HexToByteArray(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
            { 
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }
            return bytes;
        }

        private static string ByteArrayToHex(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }
    }
}
