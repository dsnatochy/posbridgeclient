﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Com.Poynt.PosBridgeClient.Model;
using Com.Poynt.Api.Model;
using System.Runtime.Serialization.Json;
using System.Text;
using Jil;
using System;

namespace UnitTests
{
    [TestClass]
    public class SerializationTests
    {
        private const string JsonObjectDirectory = "JsonObjects";
        private const string POSResponseJsonPath = @"JsonObjects/POSResponseJson.txt";


        [TestMethod]
        public void POSResponseSerializationTest_Sucess()
        {
            string json = File.ReadAllText(POSResponseJsonPath);
            POSResponse salePosResponse;
            using (var input = new StringReader(json))
            {
                salePosResponse = JSON.Deserialize<POSResponse>(input, Options.ISO8601);
            }

            Assert.IsNotNull(salePosResponse);
        }


        // Deserializing into a dynamic object works much faster .5 second vs. 10-12
        // Use dynamic where possible
        [TestMethod]
        public void POSResponseSerializerDynamicTest_Success()
        {
            string json = File.ReadAllText(POSResponseJsonPath);
            dynamic salePosResponse;
            //You can pass string as input or a StringReader object (see next test)
            //Passing string is much slower
            salePosResponse = JSON.DeserializeDynamic(json, Options.ISO8601);

            Assert.IsNotNull(salePosResponse);
        }

        [TestMethod]
        public void TransactionSerializationTest_Success()
        {
            string path = Path.Combine(JsonObjectDirectory, "TransactionJson.txt");
            string json = File.ReadAllText(path);

            Transaction transaction;

            using (var input = new StringReader(json))
            {
                transaction = JSON.Deserialize<Transaction>(input, Options.ISO8601);
            }

            Assert.IsNotNull(transaction);

        }

        [TestMethod]
        public void TransactionAmountSerializationTest_Success()
        {
            string path = Path.Combine(JsonObjectDirectory, "TransactionAmountsJson.txt");
            string json = File.ReadAllText(path);
            TransactionAmounts ta;

            using (var input = new StringReader(json))
            {
                ta = JSON.Deserialize<TransactionAmounts>(input);
            }

            Assert.IsNotNull(ta);
        }

        [TestMethod]
        public void SalePosRequestSerializationTest_Success()
        {
            int timeout = 60000; // deault timeout 60 seconds
            POSRequest salePosRequest = new POSRequest();
            Payment payment = new Payment();
            payment.amount = 1000;
            payment.currency = "USD";
            // to allow multiple tenders on a single transaction
            payment.multiTender = true;
            // set custom reference id (could be POS reference)
            payment.referenceId = Guid.NewGuid().ToString();

            salePosRequest.payment = payment;
            // for idempotency, has to be UUID
            salePosRequest.poyntRequestId = Guid.NewGuid().ToString();
            // set client timeout to 60 seconds
            salePosRequest.timeout = timeout;

            string saleRequest = JSON.Serialize<POSRequest>(salePosRequest, Options.ISO8601);

            Assert.IsFalse(string.IsNullOrEmpty(saleRequest));
        }
    }
}
