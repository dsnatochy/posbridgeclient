using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveryClient
	{
		[DataMember(IsRequired = false)]
		public string serial {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string build {get; set;}
		[DataMember(IsRequired = false)]
		public string agent {get; set;}
		[DataMember(IsRequired = false)]
		public string agentVersion {get; set;}
		[DataMember(IsRequired = false)]
		public string gpsLocation {get; set;}
		[DataMember(IsRequired = false)]
		public string buildType {get; set;}
		[DataMember(IsRequired = false)]
		public string keyState {get; set;}
		[DataMember(IsRequired = false)]
		public string tamperState {get; set;}
	}
}
