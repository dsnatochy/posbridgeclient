using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Business
	{
		[DataMember(IsRequired = false)]
		public Address address {get; set;}
		[DataMember(IsRequired = false)]
		public bool? mockProcessor {get; set;}
		[DataMember(IsRequired = false)]
		public BusinessStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public BusinessType type {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset activeSince {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public List<Store> stores {get; set;}
		[DataMember(IsRequired = false)]
		public long? _id {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> attributes {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> processorData {get; set;}
		[DataMember(IsRequired = false)]
		public Phone phone {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public Processor processor {get; set;}
		[DataMember(IsRequired = false)]
		public string externalMerchantId {get; set;}
		[DataMember(IsRequired = false)]
		public string legalName {get; set;}
		[DataMember(IsRequired = false)]
		public string businessUrl {get; set;}
		[DataMember(IsRequired = false)]
		public string emailAddress {get; set;}
		[DataMember(IsRequired = false)]
		public string doingBusinessAs {get; set;}
		[DataMember(IsRequired = false)]
		public string description {get; set;}
		[DataMember(IsRequired = false)]
		public string industryType {get; set;}
		[DataMember(IsRequired = false)]
		public string mcc {get; set;}
		[DataMember(IsRequired = false)]
		public string sic {get; set;}
		[DataMember(IsRequired = false)]
		public string timezone {get; set;}
		[DataMember(IsRequired = false)]
		public string logoUrl {get; set;}
		[DataMember(IsRequired = false)]
		public Guid id {get; set;}
	}
}
