using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Stay
	{
		[DataMember(IsRequired = false)]
		public bool? taxExempted {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public ClientContext context {get; set;}
		[DataMember(IsRequired = false)]
		public List<Discount> discounts {get; set;}
		[DataMember(IsRequired = false)]
		public List<Fee> fees {get; set;}
		[DataMember(IsRequired = false)]
		public List<Link> links {get; set;}
		[DataMember(IsRequired = false)]
		public List<OrderItem> items {get; set;}
		[DataMember(IsRequired = false)]
		public List<Transaction> transactions {get; set;}
		[DataMember(IsRequired = false)]
		public long? _id {get; set;}
		[DataMember(IsRequired = false)]
		public long? customerUserId {get; set;}
		[DataMember(IsRequired = false)]
		public OrderAmounts amounts {get; set;}
		[DataMember(IsRequired = false)]
		public OrderStatuses statuses {get; set;}
		[DataMember(IsRequired = false)]
		public StayType stayType {get; set;}
		[DataMember(IsRequired = false)]
		public string orderNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string notes {get; set;}
		[DataMember(IsRequired = false)]
		public Guid id {get; set;}
		[DataMember(IsRequired = false)]
		public Guid parentId {get; set;}
	}
}
