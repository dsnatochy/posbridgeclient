using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class SelectableVariation
	{
		[DataMember(IsRequired = false)]
		public List<SelectableValue> values {get; set;}
		[DataMember(IsRequired = false)]
		public string attribute {get; set;}
		[DataMember(IsRequired = false)]
		public string cardinality {get; set;}
	}
}
