using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CustomerScore
	{
		[DataMember(IsRequired = false)]
		public double score {get; set;}
		[DataMember(IsRequired = false)]
		public CustomerScoreType type {get; set;}
	}
}
