using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CashRegisterOrderItem
	{
		[DataMember(IsRequired = false)]
		public int? amount {get; set;}
		[DataMember(IsRequired = false)]
		public int? price {get; set;}
		[DataMember(IsRequired = false)]
		public int? quantity {get; set;}
		[DataMember(IsRequired = false)]
		public int? runningSubtotal {get; set;}
		[DataMember(IsRequired = false)]
		public int? itemTypeCount {get; set;}
		[DataMember(IsRequired = false)]
		public int? tax {get; set;}
		[DataMember(IsRequired = false)]
		public List<CashRegisterOrderItemContains> contains {get; set;}
		[DataMember(IsRequired = false)]
		public List<string> wikipediaTagsList {get; set;}
		[DataMember(IsRequired = false)]
		public string category {get; set;}
		[DataMember(IsRequired = false)]
		public string cmd {get; set;}
		[DataMember(IsRequired = false)]
		public string itemCategory {get; set;}
		[DataMember(IsRequired = false)]
		public string itemClean {get; set;}
		[DataMember(IsRequired = false)]
		public string itemType {get; set;}
		[DataMember(IsRequired = false)]
		public string msg {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string registerItemLong {get; set;}
	}
}
