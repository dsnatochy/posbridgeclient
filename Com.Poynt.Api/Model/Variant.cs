using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Variant
	{
		[DataMember(IsRequired = false)]
		public bool? defaultVariant {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount price {get; set;}
		[DataMember(IsRequired = false)]
		public List<Inventory> inventory {get; set;}
		[DataMember(IsRequired = false)]
		public List<ProductVariation> variations {get; set;}
		[DataMember(IsRequired = false)]
		public List<SelectableVariation> selectableVariations {get; set;}
		[DataMember(IsRequired = false)]
		public string sku {get; set;}
	}
}
