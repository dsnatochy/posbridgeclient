using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TerminalId
	{
		[DataMember(IsRequired = false)]
		public Business business {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset activatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> properties {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public Processor processor {get; set;}
		[DataMember(IsRequired = false)]
		public Store store {get; set;}
		[DataMember(IsRequired = false)]
		public string tid {get; set;}
		[DataMember(IsRequired = false)]
		public string mid {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceSerial {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceName {get; set;}
		[DataMember(IsRequired = false)]
		public string catalogId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid id {get; set;}
	}
}
