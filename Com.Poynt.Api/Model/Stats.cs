using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Stats
	{
		[DataMember(IsRequired = false)]
		public List<Stats> breakDowns {get; set;}
		[DataMember(IsRequired = false)]
		public List<StatsItem> items {get; set;}
		[DataMember(IsRequired = false)]
		public long? businessId {get; set;}
		[DataMember(IsRequired = false)]
		public long? storeId {get; set;}
		[DataMember(IsRequired = false)]
		public long? terminalId {get; set;}
		[DataMember(IsRequired = false)]
		public long? date {get; set;}
		[DataMember(IsRequired = false)]
		public StatTimeUnit timeUnit {get; set;}
		[DataMember(IsRequired = false)]
		public StatsFacet facet {get; set;}
		[DataMember(IsRequired = false)]
		public StatsLayout layout {get; set;}
		[DataMember(IsRequired = false)]
		public string breakDownId {get; set;}
		[DataMember(IsRequired = false)]
		public string type {get; set;}
		[DataMember(IsRequired = false)]
		public string label {get; set;}
		[DataMember(IsRequired = false)]
		public string heading {get; set;}
	}
}
