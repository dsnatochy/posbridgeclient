using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CancelDetails
	{
		[DataMember(IsRequired = false)]
		public ProcessorResponse cancelProcessorResponse {get; set;}
		[DataMember(IsRequired = false)]
		public string storeDeviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string developerMessage {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAction originalTransactionAction {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionStatus originalTransactionStatus {get; set;}
		[DataMember(IsRequired = false)]
		public Guid originalTransactionId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid cancelTransactionId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
