using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CustomerBusinessPreferences
	{
		[DataMember(IsRequired = false)]
		public bool? useCardOnFile {get; set;}
		[DataMember(IsRequired = false)]
		public bool? emailReceipt {get; set;}
		[DataMember(IsRequired = false)]
		public bool? printPaperReceipt {get; set;}
		[DataMember(IsRequired = false)]
		public long? preferredCardId {get; set;}
	}
}
