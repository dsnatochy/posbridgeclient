using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveredEndpoint
	{
		[DataMember(IsRequired = false)]
		public int? httpConnectTimeout {get; set;}
		[DataMember(IsRequired = false)]
		public int? httpReadTimeout {get; set;}
		[DataMember(IsRequired = false)]
		public string address {get; set;}
	}
}
