using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum EntryMode 
	{
		KEYED,
		TRACK_DATA_FROM_MAGSTRIPE,
		CONTACTLESS_MAGSTRIPE,
		INTEGRATED_CIRCUIT_CARD,
		CONTACTLESS_INTEGRATED_CIRCUIT_CARD,
	}
}
