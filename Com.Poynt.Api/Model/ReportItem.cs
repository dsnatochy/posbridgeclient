using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ReportItem
	{
		[DataMember(IsRequired = false)]
		public long? start_date {get; set;}
		[DataMember(IsRequired = false)]
		public long? end_date {get; set;}
		[DataMember(IsRequired = false)]
		public long? generated_at {get; set;}
		[DataMember(IsRequired = false)]
		public string report_type {get; set;}
		[DataMember(IsRequired = false)]
		public string title {get; set;}
		[DataMember(IsRequired = false)]
		public string google_url {get; set;}
		[DataMember(IsRequired = false)]
		public string s3_url {get; set;}
	}
}
