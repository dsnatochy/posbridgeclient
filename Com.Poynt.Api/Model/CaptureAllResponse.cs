using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CaptureAllResponse
	{
		[DataMember(IsRequired = false)]
		public bool? processingCloseBatch {get; set;}
		[DataMember(IsRequired = false)]
		public List<CaptureAllAccepted> acceptedList {get; set;}
	}
}
