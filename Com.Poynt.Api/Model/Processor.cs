using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum Processor 
	{
		CHASE_PAYMENTECH,
		REDE,
		EVO,
		FIRST_DATA,
		GLOBAL_PAYMENTS,
		HEARTLAND_PAYMENT_SYSTEM,
		ELAVON,
		MERCURY,
		MONERIS,
		PAYPAL,
		STRIPE,
		TSYS,
		VANTIV,
		WORLDPAY,
		EPX,
		BRIDGEPAY,
		CREDITCALL,
		NA_BANCARD,
		MOCK,
	}
}
