using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class AuthEval
	{
		[DataMember(IsRequired = false)]
		public List<string> ops {get; set;}
		[DataMember(IsRequired = false)]
		public string resource {get; set;}
		[DataMember(IsRequired = false)]
		public string status {get; set;}
	}
}
