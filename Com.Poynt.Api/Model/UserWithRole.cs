using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class UserWithRole
	{
		[DataMember(IsRequired = false)]
		public User user {get; set;}
		[DataMember(IsRequired = false)]
		public UserRole role {get; set;}
	}
}
