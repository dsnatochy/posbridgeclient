using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum FulfillmentInstruction 
	{
		NONE,
		PICKUP_INSTORE,
		SHIP_TO,
	}
}
