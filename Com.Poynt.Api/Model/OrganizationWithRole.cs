using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class OrganizationWithRole
	{
		[DataMember(IsRequired = false)]
		public Organization organization {get; set;}
		[DataMember(IsRequired = false)]
		public UserRole role {get; set;}
	}
}
