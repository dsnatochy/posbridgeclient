using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class BulkOrdersRequest
	{
		[DataMember(IsRequired = false)]
		public int? orderCountPerTimeWindow {get; set;}
		[DataMember(IsRequired = false)]
		public int? timeWindowInSec {get; set;}
		[DataMember(IsRequired = false)]
		public int? cashOrdersPercent {get; set;}
		[DataMember(IsRequired = false)]
		public List<Card> cards {get; set;}
		[DataMember(IsRequired = false)]
		public long? startTimeSec {get; set;}
		[DataMember(IsRequired = false)]
		public long? endTimeSec {get; set;}
		[DataMember(IsRequired = false)]
		public long? employeeUserId {get; set;}
		[DataMember(IsRequired = false)]
		public string storeDeviceId {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAction cardTxnsAction {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
