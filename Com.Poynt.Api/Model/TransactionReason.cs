using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TransactionReason
	{
		[DataMember(IsRequired = false)]
		public List<TransactionReasonProgramFor> programFor {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionReasonProgram program {get; set;}
	}
}
