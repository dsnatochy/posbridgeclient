using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ErrorInfo
	{
		[DataMember(IsRequired = false)]
		public Code code {get; set;}
		[DataMember(IsRequired = false)]
		public int? httpStatus {get; set;}
		[DataMember(IsRequired = false)]
		public string message {get; set;}
		[DataMember(IsRequired = false)]
		public string developerMessage {get; set;}
		[DataMember(IsRequired = false)]
		public string moreInfo {get; set;}
		[DataMember(IsRequired = false)]
		public string requestId {get; set;}
	}
}
