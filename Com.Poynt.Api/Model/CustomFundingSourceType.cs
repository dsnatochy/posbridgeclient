using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum CustomFundingSourceType 
	{
		GIFT_CARD,
		BITCOIN,
		CHEQUE,
		VOUCHER,
		REWARD,
		COUPON,
		GIFT_CERTIFICATE,
		QR_CODE,
		OTHER,
	}
}
