using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveredCloudMessageService
	{
		[DataMember(IsRequired = false)]
		public DiscoveredEndpoint endPoint {get; set;}
	}
}
