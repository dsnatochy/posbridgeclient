using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class PriceBookItem
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount listPrice {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount salesPrice {get; set;}
		[DataMember(IsRequired = false)]
		public PriceStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string priceBookId {get; set;}
		[DataMember(IsRequired = false)]
		public string productId {get; set;}
	}
}
