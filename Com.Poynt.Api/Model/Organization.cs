using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Organization
	{
		[DataMember(IsRequired = false)]
		public bool? distributor {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public List<UserWithRole> members {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> attributes {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string url {get; set;}
		[DataMember(IsRequired = false)]
		public string emailAddress {get; set;}
		[DataMember(IsRequired = false)]
		public string description {get; set;}
		[DataMember(IsRequired = false)]
		public Guid id {get; set;}
	}
}
