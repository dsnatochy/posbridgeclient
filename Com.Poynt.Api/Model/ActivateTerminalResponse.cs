using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ActivateTerminalResponse
	{
		[DataMember(IsRequired = false)]
		public bool? usingExistingCredentials {get; set;}
		[DataMember(IsRequired = false)]
		public bool? alreadyActivated {get; set;}
		[DataMember(IsRequired = false)]
		public TokenResponse tokenResponse {get; set;}
	}
}
