using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class AdjustmentRecord
	{
		[DataMember(IsRequired = false)]
		public bool? signatureCaptured {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public int? sequence {get; set;}
		[DataMember(IsRequired = false)]
		public ProcessorResponse processorResponse {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts amounts {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts amountChanges {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionReason reason {get; set;}
		[DataMember(IsRequired = false)]
		public byte[] signature {get; set;}
	}
}
