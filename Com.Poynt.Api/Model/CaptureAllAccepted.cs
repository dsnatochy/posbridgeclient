using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CaptureAllAccepted
	{
		[DataMember(IsRequired = false)]
		public string requestId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid authTransactionId {get; set;}
	}
}
