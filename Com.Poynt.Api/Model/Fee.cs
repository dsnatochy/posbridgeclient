using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Fee
	{
		[DataMember(IsRequired = false)]
		public float? percentage {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public long? amount {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
	}
}
