using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveredTerminal
	{
		[DataMember(IsRequired = false)]
		public List<DiscoveredActivation> activations {get; set;}
		[DataMember(IsRequired = false)]
		public List<DiscoveredBusiness> potentialBusinesses {get; set;}
	}
}
