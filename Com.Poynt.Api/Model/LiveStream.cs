using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class LiveStream
	{
		[DataMember(IsRequired = false)]
		public string type {get; set;}
		[DataMember(IsRequired = false)]
		public string body {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
	}
}
