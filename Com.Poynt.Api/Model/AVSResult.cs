using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class AVSResult
	{
		[DataMember(IsRequired = false)]
		public AVSResultType addressResult {get; set;}
		[DataMember(IsRequired = false)]
		public AVSResultType postalCodeResult {get; set;}
		[DataMember(IsRequired = false)]
		public AVSResultType stateResult {get; set;}
		[DataMember(IsRequired = false)]
		public AVSResultType countryResult {get; set;}
		[DataMember(IsRequired = false)]
		public AVSResultType phoneResult {get; set;}
		[DataMember(IsRequired = false)]
		public AVSResultType cardHolderNameResult {get; set;}
		[DataMember(IsRequired = false)]
		public AVSResultType cityResult {get; set;}
		[DataMember(IsRequired = false)]
		public string actualResult {get; set;}
	}
}
