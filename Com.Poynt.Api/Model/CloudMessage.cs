using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CloudMessage
	{
		[DataMember(IsRequired = false)]
		public ComponentName recipient {get; set;}
		[DataMember(IsRequired = false)]
		public int? ttl {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string collapseKey {get; set;}
		[DataMember(IsRequired = false)]
		public string data {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string serialNum {get; set;}
		[DataMember(IsRequired = false)]
		public string sender {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
