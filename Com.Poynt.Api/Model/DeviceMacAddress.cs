using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DeviceMacAddress
	{
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public NetworkInterfaceType networkInterface {get; set;}
		[DataMember(IsRequired = false)]
		public string macAddress {get; set;}
	}
}
