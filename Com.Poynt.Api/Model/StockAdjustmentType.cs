using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum StockAdjustmentType 
	{
		STOCK_ORDER,
		STOCK_TAKE,
		STOCK_TRANSFER,
		STOCK_RETURN,
	}
}
