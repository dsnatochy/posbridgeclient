using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ComponentName
	{
		[DataMember(IsRequired = false)]
		public string packageName {get; set;}
		[DataMember(IsRequired = false)]
		public string className {get; set;}
	}
}
