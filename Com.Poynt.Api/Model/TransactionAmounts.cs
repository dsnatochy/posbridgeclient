using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TransactionAmounts
	{
		[DataMember(IsRequired = false)]
		public bool? customerOptedNoTip {get; set;}
		[DataMember(IsRequired = false)]
		public long? transactionAmount {get; set;}
		[DataMember(IsRequired = false)]
		public long? orderAmount {get; set;}
		[DataMember(IsRequired = false)]
		public long? tipAmount {get; set;}
		[DataMember(IsRequired = false)]
		public long? cashbackAmount {get; set;}
		[DataMember(IsRequired = false)]
		public string currency {get; set;}
	}
}
