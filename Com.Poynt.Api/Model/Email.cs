using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Email
	{
		[DataMember(IsRequired = false)]
		public bool? primary {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public EmailStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public EmailType type {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public string emailAddress {get; set;}
	}
}
