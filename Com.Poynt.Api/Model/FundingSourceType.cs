using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum FundingSourceType 
	{
		CHEQUE,
		CUSTOM_FUNDING_SOURCE,
		CREDIT_DEBIT,
		CASH,
	}
}
