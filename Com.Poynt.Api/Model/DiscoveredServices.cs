using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveredServices
	{
		[DataMember(IsRequired = false)]
		public DiscoveredBusinessService businessService {get; set;}
		[DataMember(IsRequired = false)]
		public DiscoveredCatalogService catalogService {get; set;}
		[DataMember(IsRequired = false)]
		public DiscoveredCloudMessageService cloudMessageService {get; set;}
		[DataMember(IsRequired = false)]
		public DiscoveredPaymentService paymentService {get; set;}
	}
}
