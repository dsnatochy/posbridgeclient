using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class PoyntLoyaltyReward
	{
		[DataMember(IsRequired = false)]
		public bool? newReward {get; set;}
		[DataMember(IsRequired = false)]
		public long? businessLoyaltyId {get; set;}
		[DataMember(IsRequired = false)]
		public long? rewardId {get; set;}
		[DataMember(IsRequired = false)]
		public long? value {get; set;}
		[DataMember(IsRequired = false)]
		public string status {get; set;}
		[DataMember(IsRequired = false)]
		public string type {get; set;}
		[DataMember(IsRequired = false)]
		public string rewardDescription {get; set;}
		[DataMember(IsRequired = false)]
		public string preText {get; set;}
		[DataMember(IsRequired = false)]
		public string postText {get; set;}
	}
}
