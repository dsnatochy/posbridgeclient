using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CashRegisterOrderItemContains
	{
		[DataMember(IsRequired = false)]
		public float? amount {get; set;}
		[DataMember(IsRequired = false)]
		public string amountType {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
	}
}
