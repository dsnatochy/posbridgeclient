using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CatalogList
	{
		[DataMember(IsRequired = false)]
		public List<Catalog> catalogs {get; set;}
		[DataMember(IsRequired = false)]
		public List<Link> links {get; set;}
	}
}
