using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Phone
	{
		[DataMember(IsRequired = false)]
		public bool? primaryDayTime {get; set;}
		[DataMember(IsRequired = false)]
		public bool? primaryEvening {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public PhoneStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public PhoneType type {get; set;}
		[DataMember(IsRequired = false)]
		public string ituCountryCode {get; set;}
		[DataMember(IsRequired = false)]
		public string areaCode {get; set;}
		[DataMember(IsRequired = false)]
		public string localPhoneNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string extensionNumber {get; set;}
	}
}
