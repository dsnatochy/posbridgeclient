using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveredBusiness
	{
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> challengeQuestions {get; set;}
		[DataMember(IsRequired = false)]
		public string activationToken {get; set;}
		[DataMember(IsRequired = false)]
		public string businessName {get; set;}
		[DataMember(IsRequired = false)]
		public string storeName {get; set;}
		[DataMember(IsRequired = false)]
		public string storeAddress {get; set;}
	}
}
