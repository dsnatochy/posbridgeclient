using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class EBTDetails
	{
		[DataMember(IsRequired = false)]
		public EBTType type {get; set;}
		[DataMember(IsRequired = false)]
		public string electronicVoucherSerialNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string electronicVoucherApprovalCode {get; set;}
	}
}
