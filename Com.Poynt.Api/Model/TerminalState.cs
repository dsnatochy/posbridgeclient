using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TerminalState
	{
		[DataMember(IsRequired = false)]
		public string gpsLocation {get; set;}
		[DataMember(IsRequired = false)]
		public string keysState {get; set;}
		[DataMember(IsRequired = false)]
		public string tamperState {get; set;}
		[DataMember(IsRequired = false)]
		public string androidBuildType {get; set;}
		[DataMember(IsRequired = false)]
		public string bootSecurity {get; set;}
	}
}
