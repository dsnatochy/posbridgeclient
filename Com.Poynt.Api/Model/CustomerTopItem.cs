using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CustomerTopItem
	{
		[DataMember(IsRequired = false)]
		public double count {get; set;}
		[DataMember(IsRequired = false)]
		public long? productId {get; set;}
		[DataMember(IsRequired = false)]
		public long? variationId {get; set;}
		[DataMember(IsRequired = false)]
		public long? firstPurchasedAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? lastPurchasedAt {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public UnitOfMeasure countUnit {get; set;}
	}
}
