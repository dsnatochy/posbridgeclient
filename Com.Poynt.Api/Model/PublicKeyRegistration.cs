using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class PublicKeyRegistration
	{
		[DataMember(IsRequired = false)]
		public PublicKeyVerificationData verificationData {get; set;}
		[DataMember(IsRequired = false)]
		public string publicKey {get; set;}
		[DataMember(IsRequired = false)]
		public string serialNumber {get; set;}
	}
}
