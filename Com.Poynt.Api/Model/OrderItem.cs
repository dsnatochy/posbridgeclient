using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class OrderItem
	{
		[DataMember(IsRequired = false)]
		public bool? taxExempted {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset serviceStartAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset serviceEndAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public float? quantity {get; set;}
		[DataMember(IsRequired = false)]
		public FulfillmentInstruction fulfillmentInstruction {get; set;}
		[DataMember(IsRequired = false)]
		public int? id {get; set;}
		[DataMember(IsRequired = false)]
		public List<Discount> discounts {get; set;}
		[DataMember(IsRequired = false)]
		public List<Fee> fees {get; set;}
		[DataMember(IsRequired = false)]
		public List<OrderItemTax> taxes {get; set;}
		[DataMember(IsRequired = false)]
		public List<Variant> selectedVariants {get; set;}
		[DataMember(IsRequired = false)]
		public long? _id {get; set;}
		[DataMember(IsRequired = false)]
		public long? unitPrice {get; set;}
		[DataMember(IsRequired = false)]
		public long? discount {get; set;}
		[DataMember(IsRequired = false)]
		public long? fee {get; set;}
		[DataMember(IsRequired = false)]
		public long? tax {get; set;}
		[DataMember(IsRequired = false)]
		public OrderItemStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string productId {get; set;}
		[DataMember(IsRequired = false)]
		public string sku {get; set;}
		[DataMember(IsRequired = false)]
		public string clientNotes {get; set;}
		[DataMember(IsRequired = false)]
		public string details {get; set;}
		[DataMember(IsRequired = false)]
		public UnitOfMeasure unitOfMeasure {get; set;}
	}
}
