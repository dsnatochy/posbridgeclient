using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CardVerification
	{
		[DataMember(IsRequired = false)]
		public ClientContext context {get; set;}
		[DataMember(IsRequired = false)]
		public FundingSource fundingSource {get; set;}
		[DataMember(IsRequired = false)]
		public ProcessorResponse processorResponse {get; set;}
	}
}
