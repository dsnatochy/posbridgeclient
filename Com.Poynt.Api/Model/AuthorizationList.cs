using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class AuthorizationList
	{
		[DataMember(IsRequired = false)]
		public AuthorizationListType type {get; set;}
		[DataMember(IsRequired = false)]
		public List<Guid> ids {get; set;}
	}
}
