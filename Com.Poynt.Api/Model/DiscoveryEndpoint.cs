using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveryEndpoint
	{
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> parameters {get; set;}
		[DataMember(IsRequired = false)]
		public string address {get; set;}
		[DataMember(IsRequired = false)]
		public string version {get; set;}
	}
}
