using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ActivateStoreDeviceResponse
	{
		[DataMember(IsRequired = false)]
		public bool? alreadyActivated {get; set;}
		[DataMember(IsRequired = false)]
		public string emailSignupCode {get; set;}
	}
}
