using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class OrderReturn
	{
		[DataMember(IsRequired = false)]
		public bool? partial {get; set;}
		[DataMember(IsRequired = false)]
		public Order returnOrder {get; set;}
	}
}
