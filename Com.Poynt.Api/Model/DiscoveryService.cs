using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveryService
	{
		[DataMember(IsRequired = false)]
		public DiscoveryEndpoint endPoint {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string,string> parameters {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
	}
}
