using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum TransactionReasonProgram 
	{
		NO_SHOW,
		PURCHASE,
		CARD_DEPOSIT,
		DELAYED_CHARGE,
		EXPRESS_SERVICE,
		ASSURED_RESERVATION,
	}
}
