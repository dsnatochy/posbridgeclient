using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum BusinessAttribute 
	{
		AUTO_INVENTORY_UPDATE,
		SETTLEMENT_MODE,
		ENABLE_LOYALTY,
		EVO_VERSION,
		DISTRIBUTOR_ID,
	}
}
