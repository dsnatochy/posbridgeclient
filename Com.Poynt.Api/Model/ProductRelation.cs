using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ProductRelation
	{
		[DataMember(IsRequired = false)]
		public CurrencyAmount price {get; set;}
		[DataMember(IsRequired = false)]
		public List<string> relatedProductSku {get; set;}
		[DataMember(IsRequired = false)]
		public long? count {get; set;}
		[DataMember(IsRequired = false)]
		public ProductRelationType type {get; set;}
		[DataMember(IsRequired = false)]
		public string relatedProductId {get; set;}
	}
}
