using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TransactionAmountsSummary
	{
		[DataMember(IsRequired = false)]
		public int? authorizedCount {get; set;}
		[DataMember(IsRequired = false)]
		public int? voidedCount {get; set;}
		[DataMember(IsRequired = false)]
		public int? capturedCount {get; set;}
		[DataMember(IsRequired = false)]
		public int? refundedCount {get; set;}
		[DataMember(IsRequired = false)]
		public int? savedCount {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts authorizedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts voidedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts capturedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts refundedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts savedTotals {get; set;}
	}
}
