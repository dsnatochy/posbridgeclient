using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Store
	{
		[DataMember(IsRequired = false)]
		public Address address {get; set;}
		[DataMember(IsRequired = false)]
		public bool? fixedLocation {get; set;}
		[DataMember(IsRequired = false)]
		public bool? mockProcessor {get; set;}
		[DataMember(IsRequired = false)]
		public float? longitude {get; set;}
		[DataMember(IsRequired = false)]
		public float? latitude {get; set;}
		[DataMember(IsRequired = false)]
		public List<StoreDevice> storeDevices {get; set;}
		[DataMember(IsRequired = false)]
		public List<StoreTerminalId> storeTerminalIds {get; set;}
		[DataMember(IsRequired = false)]
		public long? _id {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> attributes {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> processorData {get; set;}
		[DataMember(IsRequired = false)]
		public Phone phone {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public Processor processor {get; set;}
		[DataMember(IsRequired = false)]
		public StoreStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public string externalStoreId {get; set;}
		[DataMember(IsRequired = false)]
		public string gatewayStoreId {get; set;}
		[DataMember(IsRequired = false)]
		public string displayName {get; set;}
		[DataMember(IsRequired = false)]
		public string currency {get; set;}
		[DataMember(IsRequired = false)]
		public string timezone {get; set;}
		[DataMember(IsRequired = false)]
		public string catalogId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid id {get; set;}
	}
}
