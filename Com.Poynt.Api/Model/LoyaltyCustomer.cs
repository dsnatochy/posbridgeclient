using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class LoyaltyCustomer
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? _id {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<IdType, string> identifiers {get; set;}
		[DataMember(IsRequired = false)]
		public ProviderVerification providerVerification {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string provider {get; set;}
		[DataMember(IsRequired = false)]
		public string firstName {get; set;}
		[DataMember(IsRequired = false)]
		public string lastName {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
	}
}
