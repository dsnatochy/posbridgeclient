using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum StayType 
	{
		GENERAL_CONTAINER,
		REGULAR_STAY,
		QUICK_STAY,
		NON_LODGING_SALE,
		NON_LODGING_NRR,
	}
}
