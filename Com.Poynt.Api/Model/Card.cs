using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Card
	{
		[DataMember(IsRequired = false)]
		public bool? encrypted {get; set;}
		[DataMember(IsRequired = false)]
		public CardType type {get; set;}
		[DataMember(IsRequired = false)]
		public FinancialInstrumentStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public int? expirationDate {get; set;}
		[DataMember(IsRequired = false)]
		public int? expirationMonth {get; set;}
		[DataMember(IsRequired = false)]
		public int? expirationYear {get; set;}
		[DataMember(IsRequired = false)]
		public List<CardKeyData> key {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public string number {get; set;}
		[DataMember(IsRequired = false)]
		public string numberFirst6 {get; set;}
		[DataMember(IsRequired = false)]
		public string numberLast4 {get; set;}
		[DataMember(IsRequired = false)]
		public string numberMasked {get; set;}
		[DataMember(IsRequired = false)]
		public string numberHashed {get; set;}
		[DataMember(IsRequired = false)]
		public string cardHolderFirstName {get; set;}
		[DataMember(IsRequired = false)]
		public string cardHolderLastName {get; set;}
		[DataMember(IsRequired = false)]
		public string cardHolderFullName {get; set;}
		[DataMember(IsRequired = false)]
		public string currency {get; set;}
		[DataMember(IsRequired = false)]
		public string track1data {get; set;}
		[DataMember(IsRequired = false)]
		public string track2data {get; set;}
		[DataMember(IsRequired = false)]
		public string track3data {get; set;}
		[DataMember(IsRequired = false)]
		public string keySerialNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string sequenceNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string serviceCode {get; set;}
		[DataMember(IsRequired = false)]
		public string encryptedExpirationDate {get; set;}
	}
}
