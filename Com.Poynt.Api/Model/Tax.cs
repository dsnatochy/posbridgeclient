using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Tax
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public double? rate {get; set;}
		[DataMember(IsRequired = false)]
		public long? amount {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string description {get; set;}
		[DataMember(IsRequired = false)]
		public string type {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
	}
}
