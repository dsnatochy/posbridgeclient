using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum UnitOfMeasure 
	{
		EACH,
		HOURS,
		DAYS,
		SECONDS,
		CRATE_OF_12,
		SIX_PACH,
		GALLON,
		LITRE,
		INCH,
		FOOT,
		MILLIMETER,
		CENTIMETER,
		METER,
		SQUARE_METER,
		CUBIC_METER,
		GRAM,
		KILOGRAM,
		POUND,
		ANNUAL,
		DEGREE_CELCIUS,
		DEGREE_FARENHEIT,
	}
}
