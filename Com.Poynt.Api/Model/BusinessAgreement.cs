using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class BusinessAgreement
	{
		[DataMember(IsRequired = false)]
		public bool? versionOutdated {get; set;}
		[DataMember(IsRequired = false)]
		public bool? current {get; set;}
		[DataMember(IsRequired = false)]
		public BusinessAgreementType type {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset acceptedAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? userId {get; set;}
		[DataMember(IsRequired = false)]
		public string version {get; set;}
	}
}
