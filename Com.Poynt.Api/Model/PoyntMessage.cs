using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class PoyntMessage
	{
		[DataMember(IsRequired = false)]
		public long? notValidAfter {get; set;}
		[DataMember(IsRequired = false)]
		public long? notValidBefore {get; set;}
		[DataMember(IsRequired = false)]
		public long? updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string message {get; set;}
		[DataMember(IsRequired = false)]
		public string source {get; set;}
		[DataMember(IsRequired = false)]
		public string launcherActivity {get; set;}
	}
}
