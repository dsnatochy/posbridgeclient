using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ProcessorResponse
	{
		[DataMember(IsRequired = false)]
		public AVSResult avsResult {get; set;}
		[DataMember(IsRequired = false)]
		public CVResult cvResult {get; set;}
		[DataMember(IsRequired = false)]
		public long? remainingBalance {get; set;}
		[DataMember(IsRequired = false)]
		public long? approvedAmount {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> emvTags {get; set;}
		[DataMember(IsRequired = false)]
		public Processor processor {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public ProcessorStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public ProviderVerification providerVerification {get; set;}
		[DataMember(IsRequired = false)]
		public string statusCode {get; set;}
		[DataMember(IsRequired = false)]
		public string statusMessage {get; set;}
		[DataMember(IsRequired = false)]
		public string transactionId {get; set;}
		[DataMember(IsRequired = false)]
		public string approvalCode {get; set;}
		[DataMember(IsRequired = false)]
		public string batchId {get; set;}
		[DataMember(IsRequired = false)]
		public string retrievalRefNum {get; set;}
		[DataMember(IsRequired = false)]
		public string cardToken {get; set;}
		[DataMember(IsRequired = false)]
		public string cvActualResult {get; set;}
	}
}
