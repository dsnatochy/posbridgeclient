using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CancelTransactionResponse
	{
		[DataMember(IsRequired = false)]
		public List<CancelDetails> transactions {get; set;}
	}
}
