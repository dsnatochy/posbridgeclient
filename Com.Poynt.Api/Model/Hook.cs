using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Hook
	{
		[DataMember(IsRequired = false)]
		public bool? active {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public List<string> eventTypes {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string applicationId {get; set;}
		[DataMember(IsRequired = false)]
		public string secret {get; set;}
		[DataMember(IsRequired = false)]
		public string deliveryUrl {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
	}
}
