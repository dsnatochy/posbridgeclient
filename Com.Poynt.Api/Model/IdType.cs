using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum IdType 
	{
		DRIVERS_LICENCE,
		EMAIL,
		PASSPORT,
		PHONE,
		NATIONAL_ID_CARD,
	}
}
