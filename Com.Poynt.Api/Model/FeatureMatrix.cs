using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class FeatureMatrix
	{
		[DataMember(IsRequired = false)]
		public bool? enabled {get; set;}
		[DataMember(IsRequired = false)]
		public ProcessorFeature processorFeature {get; set;}
	}
}
