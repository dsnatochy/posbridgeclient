using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Terminal
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public List<TerminalId> terminalIds {get; set;}
		[DataMember(IsRequired = false)]
		public string serial {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string publicKey {get; set;}
		[DataMember(IsRequired = false)]
		public string catalogId {get; set;}
	}
}
