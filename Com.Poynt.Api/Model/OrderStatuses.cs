using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class OrderStatuses
	{
		[DataMember(IsRequired = false)]
		public FulfillmentStatus fulfillmentStatus {get; set;}
		[DataMember(IsRequired = false)]
		public OrderStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionStatusSummary transactionStatusSummary {get; set;}
	}
}
