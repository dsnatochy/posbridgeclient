using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CatalogItem
	{
		[DataMember(IsRequired = false)]
		public int? displayOrder {get; set;}
		[DataMember(IsRequired = false)]
		public List<AvailableDiscount> availableDiscounts {get; set;}
		[DataMember(IsRequired = false)]
		public List<Tax> taxes {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string color {get; set;}
	}
}
