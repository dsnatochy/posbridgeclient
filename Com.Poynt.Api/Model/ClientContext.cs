using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ClientContext
	{
		[DataMember(IsRequired = false)]
		public BusinessType businessType {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset transmissionAtLocal {get; set;}
		[DataMember(IsRequired = false)]
		public long? employeeUserId {get; set;}
		[DataMember(IsRequired = false)]
		public string storeDeviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string sourceApp {get; set;}
		[DataMember(IsRequired = false)]
		public string mid {get; set;}
		[DataMember(IsRequired = false)]
		public string tid {get; set;}
		[DataMember(IsRequired = false)]
		public string mcc {get; set;}
		[DataMember(IsRequired = false)]
		public string storeAddressTerritory {get; set;}
		[DataMember(IsRequired = false)]
		public string storeAddressCity {get; set;}
		[DataMember(IsRequired = false)]
		public string storeTimezone {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionInstruction transactionInstruction {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionSource source {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
