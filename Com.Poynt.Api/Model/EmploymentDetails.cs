using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class EmploymentDetails
	{
		[DataMember(IsRequired = false)]
		public BusinessUserRole role {get; set;}
		[DataMember(IsRequired = false)]
		public long? startAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? endAt {get; set;}
	}
}
