using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class OrderAmounts
	{
		[DataMember(IsRequired = false)]
		public long? subTotal {get; set;}
		[DataMember(IsRequired = false)]
		public long? discountTotal {get; set;}
		[DataMember(IsRequired = false)]
		public long? feeTotal {get; set;}
		[DataMember(IsRequired = false)]
		public long? taxTotal {get; set;}
		[DataMember(IsRequired = false)]
		public long? netTotal {get; set;}
		[DataMember(IsRequired = false)]
		public string currency {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts authorizedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts voidedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts capturedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts refundedTotals {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts savedTotals {get; set;}
	}
}
