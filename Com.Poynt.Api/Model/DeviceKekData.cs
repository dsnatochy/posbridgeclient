using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DeviceKekData
	{
		[DataMember(IsRequired = false)]
		public string slot {get; set;}
		[DataMember(IsRequired = false)]
		public string keySerialNumber {get; set;}
	}
}
