using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Customer
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public CustomerBusinessPreferences businessPreferences {get; set;}
		[DataMember(IsRequired = false)]
		public CustomerInsights insights {get; set;}
		[DataMember(IsRequired = false)]
		public List<Card> cards {get; set;}
		[DataMember(IsRequired = false)]
		public List<Device> devices {get; set;}
		[DataMember(IsRequired = false)]
		public List<LoyaltyCustomer> loyaltyCustomers {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<AddressType, Address> addresses {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<EmailType, Email> emails {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<PhoneType, Phone> phones {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> attributes {get; set;}
		[DataMember(IsRequired = false)]
		public string firstName {get; set;}
		[DataMember(IsRequired = false)]
		public string lastName {get; set;}
		[DataMember(IsRequired = false)]
		public string middleName {get; set;}
		[DataMember(IsRequired = false)]
		public string middleInitial {get; set;}
		[DataMember(IsRequired = false)]
		public string nickName {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
	}
}
