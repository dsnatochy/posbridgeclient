using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class InventoryAdjustmentItem
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount unitCost {get; set;}
		[DataMember(IsRequired = false)]
		public long? adjustCount {get; set;}
		[DataMember(IsRequired = false)]
		public string investmentAdjustmentId {get; set;}
		[DataMember(IsRequired = false)]
		public string inventoryId {get; set;}
	}
}
