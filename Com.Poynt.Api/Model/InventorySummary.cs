using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class InventorySummary
	{
		[DataMember(IsRequired = false)]
		public float? stockCount {get; set;}
		[DataMember(IsRequired = false)]
		public float? availableCount {get; set;}
		[DataMember(IsRequired = false)]
		public float? reOrderPoint {get; set;}
		[DataMember(IsRequired = false)]
		public float? reOrderLevel {get; set;}
		[DataMember(IsRequired = false)]
		public string productId {get; set;}
		[DataMember(IsRequired = false)]
		public string productName {get; set;}
		[DataMember(IsRequired = false)]
		public string productShortCode {get; set;}
		[DataMember(IsRequired = false)]
		public string sku {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
