using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CurrencyAmount
	{
		[DataMember(IsRequired = false)]
		public long? amount {get; set;}
		[DataMember(IsRequired = false)]
		public string currency {get; set;}
	}
}
