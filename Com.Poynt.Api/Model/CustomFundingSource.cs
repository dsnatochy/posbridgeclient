using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CustomFundingSource
	{
		[DataMember(IsRequired = false)]
		public CustomFundingSourceType type {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string provider {get; set;}
		[DataMember(IsRequired = false)]
		public string processor {get; set;}
		[DataMember(IsRequired = false)]
		public string accountId {get; set;}
		[DataMember(IsRequired = false)]
		public string description {get; set;}
	}
}
