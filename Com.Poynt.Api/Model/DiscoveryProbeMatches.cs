using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveryProbeMatches
	{
		[DataMember(IsRequired = false)]
		public bool? mockProcessor {get; set;}
		[DataMember(IsRequired = false)]
		public DiscoveryClient client {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<ProcessorFeature, bool?> processorFeatureMap {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, DiscoveryService> services {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public Processor processor {get; set;}
	}
}
