using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class PublicKeyVerificationData
	{
		[DataMember(IsRequired = false)]
		public string keySerialNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string mac {get; set;}
	}
}
