using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ActiveTime
	{
		[DataMember(IsRequired = false)]
		public bool? repeat {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset startAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset endAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? startHour {get; set;}
		[DataMember(IsRequired = false)]
		public long? endHour {get; set;}
		[DataMember(IsRequired = false)]
		public RepeatType repeatType {get; set;}
		[DataMember(IsRequired = false)]
		public long[] every {get; set;}
	}
}
