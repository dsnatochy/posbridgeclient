using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DebitEBTReEntry
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset origCreatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public string origNetworkId {get; set;}
		[DataMember(IsRequired = false)]
		public string origTraceNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string origRetrievalRefNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string origApprovalCode {get; set;}
	}
}
