using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveredActivation
	{
		[DataMember(IsRequired = false)]
		public bool? mockProcessor {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<ProcessorFeature, bool?> paymentFeatureMap {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public Processor processor {get; set;}
		[DataMember(IsRequired = false)]
		public string businessName {get; set;}
		[DataMember(IsRequired = false)]
		public string storeName {get; set;}
		[DataMember(IsRequired = false)]
		public string storeAddress {get; set;}
		[DataMember(IsRequired = false)]
		public string mid {get; set;}
		[DataMember(IsRequired = false)]
		public string tid {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid catalogId {get; set;}
	}
}
