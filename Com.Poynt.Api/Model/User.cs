using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class User
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public List<Business> businesses {get; set;}
		[DataMember(IsRequired = false)]
		public List<OrganizationWithRole> organizations {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public string firstName {get; set;}
		[DataMember(IsRequired = false)]
		public string lastName {get; set;}
		[DataMember(IsRequired = false)]
		public string middleName {get; set;}
		[DataMember(IsRequired = false)]
		public UserCredential credential {get; set;}
	}
}
