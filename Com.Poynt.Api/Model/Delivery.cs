using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Delivery
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public DeliveryStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public int? attempt {get; set;}
		[DataMember(IsRequired = false)]
		public List<Link> links {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string deliveryUrl {get; set;}
		[DataMember(IsRequired = false)]
		public string hookId {get; set;}
		[DataMember(IsRequired = false)]
		public string applicationId {get; set;}
		[DataMember(IsRequired = false)]
		public string resource {get; set;}
		[DataMember(IsRequired = false)]
		public string resourceId {get; set;}
		[DataMember(IsRequired = false)]
		public string eventType {get; set;}
		[DataMember(IsRequired = false)]
		public string secret {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
