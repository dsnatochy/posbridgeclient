using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class OrderItemTax
	{
		[DataMember(IsRequired = false)]
		public bool? taxExempted {get; set;}
		[DataMember(IsRequired = false)]
		public bool? catalogLevel {get; set;}
		[DataMember(IsRequired = false)]
		public long? amount {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string type {get; set;}
	}
}
