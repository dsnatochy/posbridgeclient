using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Application
	{
		[DataMember(IsRequired = false)]
		public ApplicationStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public ApplicationType type {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public List<ApplicationPermission> permissions {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string applicationUrn {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string description {get; set;}
		[DataMember(IsRequired = false)]
		public string publicKey {get; set;}
		[DataMember(IsRequired = false)]
		public string privateKey {get; set;}
		[DataMember(IsRequired = false)]
		public string category {get; set;}
		[DataMember(IsRequired = false)]
		public string websiteUrl {get; set;}
		[DataMember(IsRequired = false)]
		public string email {get; set;}
		[DataMember(IsRequired = false)]
		public string privacyPolicyUrl {get; set;}
		[DataMember(IsRequired = false)]
		public string returnUrl {get; set;}
		[DataMember(IsRequired = false)]
		public string packageName {get; set;}
		[DataMember(IsRequired = false)]
		public Guid organizationId {get; set;}
	}
}
