using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class BusinessUser
	{
		[DataMember(IsRequired = false)]
		public BusinessUserStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public EmploymentDetails employmentDetails {get; set;}
		[DataMember(IsRequired = false)]
		public List<Card> cards {get; set;}
		[DataMember(IsRequired = false)]
		public List<UserCredential> credentials {get; set;}
		[DataMember(IsRequired = false)]
		public long? userId {get; set;}
		[DataMember(IsRequired = false)]
		public long? startDate {get; set;}
		[DataMember(IsRequired = false)]
		public long? endDate {get; set;}
		[DataMember(IsRequired = false)]
		public string firstName {get; set;}
		[DataMember(IsRequired = false)]
		public string lastName {get; set;}
		[DataMember(IsRequired = false)]
		public string middleName {get; set;}
		[DataMember(IsRequired = false)]
		public string middleInitial {get; set;}
		[DataMember(IsRequired = false)]
		public string nickName {get; set;}
		[DataMember(IsRequired = false)]
		public string email {get; set;}
		[DataMember(IsRequired = false)]
		public string emailSignupCode {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
	}
}
