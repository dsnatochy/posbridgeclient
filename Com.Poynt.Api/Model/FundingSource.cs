using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class FundingSource
	{
		[DataMember(IsRequired = false)]
		public bool? debit {get; set;}
		[DataMember(IsRequired = false)]
		public Card card {get; set;}
		[DataMember(IsRequired = false)]
		public CustomFundingSource customFundingSource {get; set;}
		[DataMember(IsRequired = false)]
		public DebitEBTReEntry debitEBTReEntryDetails {get; set;}
		[DataMember(IsRequired = false)]
		public EBTDetails ebtDetails {get; set;}
		[DataMember(IsRequired = false)]
		public EMVData emvData {get; set;}
		[DataMember(IsRequired = false)]
		public ExchangeRate exchangeRate {get; set;}
		[DataMember(IsRequired = false)]
		public FundingSourceAccountType accountType {get; set;}
		[DataMember(IsRequired = false)]
		public FundingSourceEntryDetails entryDetails {get; set;}
		[DataMember(IsRequired = false)]
		public FundingSourceType type {get; set;}
		[DataMember(IsRequired = false)]
		public VerificationData verificationData {get; set;}
	}
}
