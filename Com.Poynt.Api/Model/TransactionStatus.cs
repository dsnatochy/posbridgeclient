using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum TransactionStatus 
	{
		CREATED,
		SAVED,
		AUTHORIZED,
		PARTIALLY_CAPTURED,
		CAPTURED,
		DECLINED,
		PARTIALLY_CAPTURED_AND_PARTIALLY_REFUNDED,
		PARTIALLY_REFUNDED,
		REFUNDED,
		VOIDED,
	}
}
