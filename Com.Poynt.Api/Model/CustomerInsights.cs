using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CustomerInsights
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset since {get; set;}
		[DataMember(IsRequired = false)]
		public List<CurrencyAmount> lifetimeSpend {get; set;}
		[DataMember(IsRequired = false)]
		public List<CustomerScore> scores {get; set;}
		[DataMember(IsRequired = false)]
		public List<CustomerTopItem> topItems {get; set;}
		[DataMember(IsRequired = false)]
		public long? totalOrders {get; set;}
		[DataMember(IsRequired = false)]
		public PoyntLoyalty poyntLoyalty {get; set;}
	}
}
