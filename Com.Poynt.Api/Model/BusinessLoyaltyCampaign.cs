using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class BusinessLoyaltyCampaign
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset startOn {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset endOn {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public long? businessId {get; set;}
		[DataMember(IsRequired = false)]
		public long? storeId {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string descCampaign {get; set;}
		[DataMember(IsRequired = false)]
		public string loyaltyType {get; set;}
		[DataMember(IsRequired = false)]
		public string loyaltyConfig {get; set;}
		[DataMember(IsRequired = false)]
		public string rewardDescription {get; set;}
	}
}
