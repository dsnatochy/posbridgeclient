using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class KeyData
	{
		[DataMember(IsRequired = false)]
		public bool? encrypted {get; set;}
		[DataMember(IsRequired = false)]
		public Key id {get; set;}
		[DataMember(IsRequired = false)]
		public string value {get; set;}
		[DataMember(IsRequired = false)]
		public string version {get; set;}
	}
}
