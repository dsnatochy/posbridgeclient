using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TransactionReference
	{
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string customType {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionReferenceType type {get; set;}
	}
}
