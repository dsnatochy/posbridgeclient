using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class StoreDevice
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset lastSeenAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public List<DeviceKekData> kekDetails {get; set;}
		[DataMember(IsRequired = false)]
		public long? _id {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<BusinessAgreementType, BusinessAgreement> businessAgreements {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> processorData {get; set;}
		[DataMember(IsRequired = false)]
		public PublicKeyVerificationData publicKeyVerification {get; set;}
		[DataMember(IsRequired = false)]
		public StoreDeviceStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public StoreDeviceType type {get; set;}
		[DataMember(IsRequired = false)]
		public string externalTerminalId {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string serialNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string publicKey {get; set;}
		[DataMember(IsRequired = false)]
		public string catalogId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
