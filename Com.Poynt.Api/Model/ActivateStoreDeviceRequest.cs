using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ActivateStoreDeviceRequest
	{
		[DataMember(IsRequired = false)]
		public Business business {get; set;}
		[DataMember(IsRequired = false)]
		public BusinessUser businessUser {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, string> challengeAnswers {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public StoreDevice device {get; set;}
		[DataMember(IsRequired = false)]
		public string externalStoreId {get; set;}
		[DataMember(IsRequired = false)]
		public string activationToken {get; set;}
	}
}
