using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Link
	{
		[DataMember(IsRequired = false)]
		public string href {get; set;}
		[DataMember(IsRequired = false)]
		public string rel {get; set;}
		[DataMember(IsRequired = false)]
		public string method {get; set;}
	}
}
