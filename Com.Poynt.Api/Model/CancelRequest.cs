using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CancelRequest
	{
		[DataMember(IsRequired = false)]
		public CancelReason cancelReason {get; set;}
		[DataMember(IsRequired = false)]
		public EMVData emvData {get; set;}
	}
}
