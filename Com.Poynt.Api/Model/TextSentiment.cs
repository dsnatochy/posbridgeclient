using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TextSentiment
	{
		[DataMember(IsRequired = false)]
		public double confidence {get; set;}
		[DataMember(IsRequired = false)]
		public string mood {get; set;}
	}
}
