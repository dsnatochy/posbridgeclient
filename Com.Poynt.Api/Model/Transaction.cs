using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Transaction
	{
		[DataMember(IsRequired = false)]
		public bool? signatureCaptured {get; set;}
		[DataMember(IsRequired = false)]
		public bool? pinCaptured {get; set;}
		[DataMember(IsRequired = false)]
		public bool? adjusted {get; set;}
		[DataMember(IsRequired = false)]
		public bool? amountsAdjusted {get; set;}
		[DataMember(IsRequired = false)]
		public bool? authOnly {get; set;}
		[DataMember(IsRequired = false)]
		public bool? partiallyApproved {get; set;}
		[DataMember(IsRequired = false)]
		public bool? voided {get; set;}
		[DataMember(IsRequired = false)]
		public bool? settled {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public ClientContext context {get; set;}
		[DataMember(IsRequired = false)]
		public FundingSource fundingSource {get; set;}
		[DataMember(IsRequired = false)]
		public List<AdjustmentRecord> adjustmentHistory {get; set;}
		[DataMember(IsRequired = false)]
		public List<Link> links {get; set;}
		[DataMember(IsRequired = false)]
		public List<TransactionReference> references {get; set;}
		[DataMember(IsRequired = false)]
		public long? _id {get; set;}
		[DataMember(IsRequired = false)]
		public long? _parentId {get; set;}
		[DataMember(IsRequired = false)]
		public long? customerUserId {get; set;}
		[DataMember(IsRequired = false)]
		public Phone receiptPhone {get; set;}
		[DataMember(IsRequired = false)]
		public PoyntLoyalty poyntLoyalty {get; set;}
		[DataMember(IsRequired = false)]
		public ProcessorResponse processorResponse {get; set;}
		[DataMember(IsRequired = false)]
		public StayType stayType {get; set;}
		[DataMember(IsRequired = false)]
		public string transactionNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string receiptEmailAddress {get; set;}
		[DataMember(IsRequired = false)]
		public string notes {get; set;}
		[DataMember(IsRequired = false)]
		public string approvalCode {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAction action {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts amounts {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionReason reason {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public Guid id {get; set;}
		[DataMember(IsRequired = false)]
		public Guid parentId {get; set;}
		[DataMember(IsRequired = false)]
		public byte[] signature {get; set;}
	}
}
