using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class SelectableValue
	{
		[DataMember(IsRequired = false)]
		public bool? defaultValue {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount priceDelta {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
	}
}
