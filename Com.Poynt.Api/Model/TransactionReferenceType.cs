using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum TransactionReferenceType 
	{
		POYNT_ORDER,
		POYNT_STAY,
		CUSTOM,
	}
}
