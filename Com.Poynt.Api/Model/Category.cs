using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Category
	{
		[DataMember(IsRequired = false)]
		public int? displayOrder {get; set;}
		[DataMember(IsRequired = false)]
		public List<AvailableDiscount> availableDiscounts {get; set;}
		[DataMember(IsRequired = false)]
		public List<CatalogItem> products {get; set;}
		[DataMember(IsRequired = false)]
		public List<Tax> taxes {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string parentCategoryId {get; set;}
		[DataMember(IsRequired = false)]
		public string shortCode {get; set;}
		[DataMember(IsRequired = false)]
		public string color {get; set;}
	}
}
