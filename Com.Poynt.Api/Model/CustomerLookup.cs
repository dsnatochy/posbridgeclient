using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CustomerLookup
	{
		[DataMember(IsRequired = false)]
		public Card card {get; set;}
		[DataMember(IsRequired = false)]
		public LoyaltyCustomer loyaltyCustomer {get; set;}
	}
}
