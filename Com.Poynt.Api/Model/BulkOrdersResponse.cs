using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class BulkOrdersResponse
	{
		[DataMember(IsRequired = false)]
		public List<Guid> ordersCreated {get; set;}
		[DataMember(IsRequired = false)]
		public long? numCreated {get; set;}
	}
}
