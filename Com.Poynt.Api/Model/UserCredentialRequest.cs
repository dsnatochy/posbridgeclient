using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class UserCredentialRequest
	{
		[DataMember(IsRequired = false)]
		public CredentialType publicCredentialType {get; set;}
		[DataMember(IsRequired = false)]
		public string oldPublicCredentialValue {get; set;}
		[DataMember(IsRequired = false)]
		public string oldPrivateCredentialValue {get; set;}
		[DataMember(IsRequired = false)]
		public string newPublicCredentialValue {get; set;}
		[DataMember(IsRequired = false)]
		public string newPrivateCredentialValue {get; set;}
	}
}
