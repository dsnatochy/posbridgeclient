using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class UserCredential
	{
		[DataMember(IsRequired = false)]
		public CredentialType publicCredentialType {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public string publicCredentialValue {get; set;}
		[DataMember(IsRequired = false)]
		public string privateCredentialValue {get; set;}
		[DataMember(IsRequired = false)]
		public string privateCredentialSalt {get; set;}
	}
}
