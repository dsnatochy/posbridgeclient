using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DeviceCommand
	{
		[DataMember(IsRequired = false)]
		public DeviceType deviceType {get; set;}
		[DataMember(IsRequired = false)]
		public List<DeviceMacAddress> macAddresses {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
	}
}
