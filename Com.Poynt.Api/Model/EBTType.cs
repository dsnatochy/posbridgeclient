using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum EBTType 
	{
		CASH_BENEFIT,
		CASH_BENEFIT_CASH_WITHDRAWAL,
		FOOD_STAMP,
		FOOD_STAMP_ELECTRONIC_VOUCHER,
	}
}
