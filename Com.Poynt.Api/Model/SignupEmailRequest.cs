using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class SignupEmailRequest
	{
		[DataMember(IsRequired = false)]
		public string firstName {get; set;}
		[DataMember(IsRequired = false)]
		public string lastName {get; set;}
		[DataMember(IsRequired = false)]
		public string email {get; set;}
		[DataMember(IsRequired = false)]
		public string signupCode {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid organizationId {get; set;}
		[DataMember(IsRequired = false)]
		public UserRole organizationRole {get; set;}
	}
}
