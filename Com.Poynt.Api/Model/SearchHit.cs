using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class SearchHit
	{
		[DataMember(IsRequired = false)]
		public ComponentName intentedFor {get; set;}
		[DataMember(IsRequired = false)]
		public Link link {get; set;}
		[DataMember(IsRequired = false)]
		public SearchResourceType type {get; set;}
		[DataMember(IsRequired = false)]
		public string displayTitle {get; set;}
		[DataMember(IsRequired = false)]
		public string displaySubTitle {get; set;}
		[DataMember(IsRequired = false)]
		public string displayType {get; set;}
		[DataMember(IsRequired = false)]
		public string displayAttribute1 {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
	}
}
