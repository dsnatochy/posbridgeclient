using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Address
	{
		[DataMember(IsRequired = false)]
		public AddressStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public AddressType type {get; set;}
		[DataMember(IsRequired = false)]
		public bool? primary {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public string line1 {get; set;}
		[DataMember(IsRequired = false)]
		public string line2 {get; set;}
		[DataMember(IsRequired = false)]
		public string city {get; set;}
		[DataMember(IsRequired = false)]
		public string territory {get; set;}
		[DataMember(IsRequired = false)]
		public string postalCode {get; set;}
		[DataMember(IsRequired = false)]
		public string postalCodeExtension {get; set;}
		[DataMember(IsRequired = false)]
		public string countryCode {get; set;}
		[DataMember(IsRequired = false)]
		public TerritoryType territoryType {get; set;}
	}
}
