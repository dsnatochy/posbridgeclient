using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CaptureAllRequest
	{
		[DataMember(IsRequired = false)]
		public AuthorizationList includeExcludeList {get; set;}
		[DataMember(IsRequired = false)]
		public CaptureAllRange range {get; set;}
		[DataMember(IsRequired = false)]
		public List<CaptureRequest> captureList {get; set;}
		[DataMember(IsRequired = false)]
		public long? employeeUserId {get; set;}
		[DataMember(IsRequired = false)]
		public string storeDeviceId {get; set;}
		[DataMember(IsRequired = false)]
		public string tid {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
