using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum CardType 
	{
		AMERICAN_EXPRESS,
		MAESTRO,
		DISCOVER,
		DINERS_CLUB,
		JCB,
		MASTERCARD,
		DANKORT,
		OTHER,
		VISA,
		UNIONPAY,
		PAYPAL,
	}
}
