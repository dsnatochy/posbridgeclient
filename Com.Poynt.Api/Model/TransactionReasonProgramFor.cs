using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum TransactionReasonProgramFor 
	{
		RESTAURANT,
		GIFT_SHOP,
		MINI_BAR,
		TELEPHONE,
		LAUNDRY,
		OTHER,
	}
}
