using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class PoyntLoyalty
	{
		[DataMember(IsRequired = false)]
		public List<PoyntLoyaltyCampaign> loyalty {get; set;}
		[DataMember(IsRequired = false)]
		public List<PoyntLoyaltyReward> reward {get; set;}
		[DataMember(IsRequired = false)]
		public long? loyaltyId {get; set;}
		[DataMember(IsRequired = false)]
		public string externalId {get; set;}
	}
}
