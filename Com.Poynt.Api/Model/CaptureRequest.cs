using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CaptureRequest
	{
		[DataMember(IsRequired = false)]
		public AdjustTransactionRequest adjustTransaction {get; set;}
		[DataMember(IsRequired = false)]
		public bool? captureOfflineAuth {get; set;}
		[DataMember(IsRequired = false)]
		public string requestId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid authTransactionId {get; set;}
	}
}
