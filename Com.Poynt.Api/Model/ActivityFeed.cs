using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ActivityFeed
	{
		[DataMember(IsRequired = false)]
		public ActivityAction action {get; set;}
		[DataMember(IsRequired = false)]
		public ActivitySource source {get; set;}
		[DataMember(IsRequired = false)]
		public ActivityUser activityUser {get; set;}
		[DataMember(IsRequired = false)]
		public double rating {get; set;}
		[DataMember(IsRequired = false)]
		public bool? newMacAddress {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset actionAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public NetworkInterfaceType networkInterface {get; set;}
		[DataMember(IsRequired = false)]
		public string sensorId {get; set;}
		[DataMember(IsRequired = false)]
		public string externalId {get; set;}
		[DataMember(IsRequired = false)]
		public string text {get; set;}
		[DataMember(IsRequired = false)]
		public string imageUrl {get; set;}
		[DataMember(IsRequired = false)]
		public TextSentiment sentiment {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeId {get; set;}
	}
}
