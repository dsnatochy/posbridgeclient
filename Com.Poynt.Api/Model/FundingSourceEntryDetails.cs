using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class FundingSourceEntryDetails
	{
		[DataMember(IsRequired = false)]
		public bool? iccFallback {get; set;}
		[DataMember(IsRequired = false)]
		public CustomerPresenceStatus customerPresenceStatus {get; set;}
		[DataMember(IsRequired = false)]
		public EntryMode entryMode {get; set;}
	}
}
