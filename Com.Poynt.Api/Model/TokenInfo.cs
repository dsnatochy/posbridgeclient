using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TokenInfo
	{
		[DataMember(IsRequired = false)]
		public List<AuthEval> authorized {get; set;}
		[DataMember(IsRequired = false)]
		public Dictionary<string, Object> jwtClaims {get; set;}
		[DataMember(IsRequired = false)]
		public Guid forBusinessId {get; set;}
	}
}
