using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ExchangeRate
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset requestedAt {get; set;}
		[DataMember(IsRequired = false)]
		public long? rate {get; set;}
		[DataMember(IsRequired = false)]
		public long? ratePrecision {get; set;}
		[DataMember(IsRequired = false)]
		public long? cardAmount {get; set;}
		[DataMember(IsRequired = false)]
		public long? txnAmount {get; set;}
		[DataMember(IsRequired = false)]
		public string cardCurrency {get; set;}
		[DataMember(IsRequired = false)]
		public string txnCurrency {get; set;}
		[DataMember(IsRequired = false)]
		public string provider {get; set;}
		[DataMember(IsRequired = false)]
		public string markupPercentage {get; set;}
		[DataMember(IsRequired = false)]
		public string signature {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
	}
}
