using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class AdjustTransactionRequest
	{
		[DataMember(IsRequired = false)]
		public EMVData emvData {get; set;}
		[DataMember(IsRequired = false)]
		public List<AdjustmentRecord> adjustmentUpdates {get; set;}
		[DataMember(IsRequired = false)]
		public Phone receiptPhone {get; set;}
		[DataMember(IsRequired = false)]
		public string receiptEmailAddress {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionAmounts amounts {get; set;}
		[DataMember(IsRequired = false)]
		public TransactionReason reason {get; set;}
		[DataMember(IsRequired = false)]
		public byte[] signature {get; set;}
	}
}
