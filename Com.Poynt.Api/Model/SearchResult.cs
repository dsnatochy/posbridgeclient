using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class SearchResult
	{
		[DataMember(IsRequired = false)]
		public bool? timedout {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset startAtFilter {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset endAtFilter {get; set;}
		[DataMember(IsRequired = false)]
		public int? total {get; set;}
		[DataMember(IsRequired = false)]
		public int? limit {get; set;}
		[DataMember(IsRequired = false)]
		public List<SearchHit> hits {get; set;}
		[DataMember(IsRequired = false)]
		public List<SearchResourceType> typeFilter {get; set;}
		[DataMember(IsRequired = false)]
		public string q {get; set;}
		[DataMember(IsRequired = false)]
		public string deviceIdFilter {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessIdFilter {get; set;}
		[DataMember(IsRequired = false)]
		public Guid storeIdFilter {get; set;}
	}
}
