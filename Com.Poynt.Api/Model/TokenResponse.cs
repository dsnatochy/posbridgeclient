using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class TokenResponse
	{
		[DataMember(IsRequired = false)]
		public long? expiresIn {get; set;}
		[DataMember(IsRequired = false)]
		public string accessToken {get; set;}
		[DataMember(IsRequired = false)]
		public string refreshToken {get; set;}
		[DataMember(IsRequired = false)]
		public string scope {get; set;}
		[DataMember(IsRequired = false)]
		public TokenType tokenType {get; set;}
	}
}
