using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class PoyntLoyaltyCampaign
	{
		[DataMember(IsRequired = false)]
		public long? businessLoyaltyId {get; set;}
		[DataMember(IsRequired = false)]
		public long? points {get; set;}
		[DataMember(IsRequired = false)]
		public long? pointsRequired {get; set;}
		[DataMember(IsRequired = false)]
		public long? totalPoints {get; set;}
		[DataMember(IsRequired = false)]
		public long? totalVisits {get; set;}
		[DataMember(IsRequired = false)]
		public long? totalSpend {get; set;}
		[DataMember(IsRequired = false)]
		public long? lastIncrement {get; set;}
		[DataMember(IsRequired = false)]
		public string campaignName {get; set;}
		[DataMember(IsRequired = false)]
		public string campaignDescription {get; set;}
		[DataMember(IsRequired = false)]
		public string loyaltyType {get; set;}
		[DataMember(IsRequired = false)]
		public string tier {get; set;}
		[DataMember(IsRequired = false)]
		public string nextTier {get; set;}
		[DataMember(IsRequired = false)]
		public string loyaltyUnit {get; set;}
		[DataMember(IsRequired = false)]
		public string rewardDescription {get; set;}
	}
}
