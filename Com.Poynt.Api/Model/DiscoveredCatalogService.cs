using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class DiscoveredCatalogService
	{
		[DataMember(IsRequired = false)]
		public DiscoveredEndpoint endPoint {get; set;}
		[DataMember(IsRequired = false)]
		public int? syncFrequency {get; set;}
	}
}
