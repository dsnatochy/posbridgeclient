using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Product
	{
		[DataMember(IsRequired = false)]
		public DateTimeOffset releaseDate {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount msrp {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount price {get; set;}
		[DataMember(IsRequired = false)]
		public CurrencyAmount avgUnitCost {get; set;}
		[DataMember(IsRequired = false)]
		public List<Inventory> inventory {get; set;}
		[DataMember(IsRequired = false)]
		public List<ProductRelation> bundledProducts {get; set;}
		[DataMember(IsRequired = false)]
		public List<ProductRelation> addonProducts {get; set;}
		[DataMember(IsRequired = false)]
		public List<ProductRelation> relatedProducts {get; set;}
		[DataMember(IsRequired = false)]
		public List<string> templateOverrides {get; set;}
		[DataMember(IsRequired = false)]
		public List<string> imageUrl {get; set;}
		[DataMember(IsRequired = false)]
		public List<Tax> taxes {get; set;}
		[DataMember(IsRequired = false)]
		public List<Variant> variants {get; set;}
		[DataMember(IsRequired = false)]
		public List<Variant> selectableVariants {get; set;}
		[DataMember(IsRequired = false)]
		public List<Variation> possibleVariations {get; set;}
		[DataMember(IsRequired = false)]
		public ProductStatus status {get; set;}
		[DataMember(IsRequired = false)]
		public ProductType type {get; set;}
		[DataMember(IsRequired = false)]
		public string ean {get; set;}
		[DataMember(IsRequired = false)]
		public string upc {get; set;}
		[DataMember(IsRequired = false)]
		public string isbn {get; set;}
		[DataMember(IsRequired = false)]
		public string plu {get; set;}
		[DataMember(IsRequired = false)]
		public string asin {get; set;}
		[DataMember(IsRequired = false)]
		public string shortCode {get; set;}
		[DataMember(IsRequired = false)]
		public string name {get; set;}
		[DataMember(IsRequired = false)]
		public string description {get; set;}
		[DataMember(IsRequired = false)]
		public string specification {get; set;}
		[DataMember(IsRequired = false)]
		public string brand {get; set;}
		[DataMember(IsRequired = false)]
		public string manufacturer {get; set;}
		[DataMember(IsRequired = false)]
		public string publisher {get; set;}
		[DataMember(IsRequired = false)]
		public string studio {get; set;}
		[DataMember(IsRequired = false)]
		public string designer {get; set;}
		[DataMember(IsRequired = false)]
		public string author {get; set;}
		[DataMember(IsRequired = false)]
		public string artist {get; set;}
		[DataMember(IsRequired = false)]
		public string tags {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string productTemplateId {get; set;}
		[DataMember(IsRequired = false)]
		public string sku {get; set;}
		[DataMember(IsRequired = false)]
		public string mpn {get; set;}
		[DataMember(IsRequired = false)]
		public string styleNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string modelNumber {get; set;}
		[DataMember(IsRequired = false)]
		public Guid businessId {get; set;}
		[DataMember(IsRequired = false)]
		public UnitOfMeasure unitOfMeasure {get; set;}
	}
}
