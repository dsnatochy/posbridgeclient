using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ProductRelationList
	{
		[DataMember(IsRequired = false)]
		public List<ProductRelation> relations {get; set;}
		[DataMember(IsRequired = false)]
		public long? count {get; set;}
	}
}
