using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class OrderList
	{
		[DataMember(IsRequired = false)]
		public List<Link> links {get; set;}
		[DataMember(IsRequired = false)]
		public List<Order> orders {get; set;}
		[DataMember(IsRequired = false)]
		public long? count {get; set;}
	}
}
