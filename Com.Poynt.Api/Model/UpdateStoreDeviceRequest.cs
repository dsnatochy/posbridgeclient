using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class UpdateStoreDeviceRequest
	{
		[DataMember(IsRequired = false)]
		public bool? nullSerialNumberAndDeviceId {get; set;}
		[DataMember(IsRequired = false)]
		public bool? nullName {get; set;}
		[DataMember(IsRequired = false)]
		public bool? nullCatalogId {get; set;}
		[DataMember(IsRequired = false)]
		public Processor acquirer {get; set;}
		[DataMember(IsRequired = false)]
		public StoreDevice storeDevice {get; set;}
		[DataMember(IsRequired = false)]
		public string externalStoreId {get; set;}
	}
}
