using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class ActivityUser
	{
		[DataMember(IsRequired = false)]
		public ActivityAction action {get; set;}
		[DataMember(IsRequired = false)]
		public ActivitySource source {get; set;}
		[DataMember(IsRequired = false)]
		public ActivityUser activityUser {get; set;}
		[DataMember(IsRequired = false)]
		public ActivityUserType type {get; set;}
		[DataMember(IsRequired = false)]
		public double rating {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset createdAt {get; set;}
		[DataMember(IsRequired = false)]
		public DateTimeOffset updatedAt {get; set;}
		[DataMember(IsRequired = false)]
		public Gender gender {get; set;}
		[DataMember(IsRequired = false)]
		public long? id {get; set;}
		[DataMember(IsRequired = false)]
		public long? matchingCustomerUserId {get; set;}
		[DataMember(IsRequired = false)]
		public string externalId {get; set;}
		[DataMember(IsRequired = false)]
		public string fullName {get; set;}
		[DataMember(IsRequired = false)]
		public string firstName {get; set;}
		[DataMember(IsRequired = false)]
		public string lastName {get; set;}
		[DataMember(IsRequired = false)]
		public string middleName {get; set;}
		[DataMember(IsRequired = false)]
		public string screenName {get; set;}
		[DataMember(IsRequired = false)]
		public string photoUrl {get; set;}
		[DataMember(IsRequired = false)]
		public string homeCity {get; set;}
		[DataMember(IsRequired = false)]
		public TextSentiment sentiment {get; set;}
	}
}
