using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum PhoneType 
	{
		HOME,
		WORK,
		BUSINESS,
		MOBILE,
		FAX,
		PAGER,
		RECEIPT,
		OTHER,
	}
}
