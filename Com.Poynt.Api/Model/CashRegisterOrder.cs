using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CashRegisterOrder
	{
		[DataMember(IsRequired = false)]
		public int? change {get; set;}
		[DataMember(IsRequired = false)]
		public int? subtotal {get; set;}
		[DataMember(IsRequired = false)]
		public int? tax {get; set;}
		[DataMember(IsRequired = false)]
		public int? tenderAmount {get; set;}
		[DataMember(IsRequired = false)]
		public List<CashRegisterOrderItem> items {get; set;}
		[DataMember(IsRequired = false)]
		public string businessId {get; set;}
		[DataMember(IsRequired = false)]
		public string checkoutCmd {get; set;}
		[DataMember(IsRequired = false)]
		public string currency {get; set;}
		[DataMember(IsRequired = false)]
		public string orderNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string tenderType {get; set;}
	}
}
