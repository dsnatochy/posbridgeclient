using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class CaptureAllRange
	{
		[DataMember(IsRequired = false)]
		public CaptureAllRangeType type {get; set;}
		[DataMember(IsRequired = false)]
		public long? startTimeSec {get; set;}
		[DataMember(IsRequired = false)]
		public long? endTimeSec {get; set;}
		[DataMember(IsRequired = false)]
		public Guid firstTransactionId {get; set;}
		[DataMember(IsRequired = false)]
		public Guid lastTransactionId {get; set;}
	}
}
