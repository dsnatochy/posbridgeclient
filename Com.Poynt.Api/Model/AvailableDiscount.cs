using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class AvailableDiscount
	{
		[DataMember(IsRequired = false)]
		public ActiveTime when {get; set;}
		[DataMember(IsRequired = false)]
		public DiscountScope scope {get; set;}
		[DataMember(IsRequired = false)]
		public DiscountType type {get; set;}
		[DataMember(IsRequired = false)]
		public float? percentage {get; set;}
		[DataMember(IsRequired = false)]
		public long? fixedDiscount {get; set;}
		[DataMember(IsRequired = false)]
		public string code {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
	}
}
