using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	public enum CancelReason 
	{
		TIMEOUT,
		CARD_REMOVED,
		CHIP_DECLINE_AFTER_HOST_APPROVAL,
		PIN_PAD_NOT_AVAILABLE,
		MERCHANT_CANCELLED,
	}
}
