using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class VerificationData
	{
		[DataMember(IsRequired = false)]
		public Address cardHolderBillingAddress {get; set;}
		[DataMember(IsRequired = false)]
		public CVSkipReason cvSkipReason {get; set;}
		[DataMember(IsRequired = false)]
		public IdType additionalIdType {get; set;}
		[DataMember(IsRequired = false)]
		public string cvData {get; set;}
		[DataMember(IsRequired = false)]
		public string additionalIdRefNumber {get; set;}
		[DataMember(IsRequired = false)]
		public string pin {get; set;}
		[DataMember(IsRequired = false)]
		public string keySerialNumber {get; set;}
	}
}
