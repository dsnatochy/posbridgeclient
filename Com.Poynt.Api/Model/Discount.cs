using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class Discount
	{
		[DataMember(IsRequired = false)]
		public float? percentage {get; set;}
		[DataMember(IsRequired = false)]
		public long? amount {get; set;}
		[DataMember(IsRequired = false)]
		public ProcessorResponse processorResponse {get; set;}
		[DataMember(IsRequired = false)]
		public string customName {get; set;}
		[DataMember(IsRequired = false)]
		public string id {get; set;}
		[DataMember(IsRequired = false)]
		public string provider {get; set;}
		[DataMember(IsRequired = false)]
		public string processor {get; set;}
	}
}
