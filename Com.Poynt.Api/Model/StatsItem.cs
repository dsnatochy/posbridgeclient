using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace Com.Poynt.Api.Model
{
	[DataContract]
	public class StatsItem
	{
		[DataMember(IsRequired = false)]
		public int? value {get; set;}
		[DataMember(IsRequired = false)]
		public string label {get; set;}
	}
}
